$(function(){
	$('.statepicker.countryselect').change(function(){
		var cid = $(this).val(),
		    id = $(this).attr('id').replace('-countryselect', '');
		$.get(
		    Drupal.settings.basePath + 'statepicker/json/state/' + cid,
		    {},
			function(data){
			    var stateselect = $('#' + id + '-stateselect'),
				    i = 0;
			    stateselect.find('option').remove();
				for(i in data) {
				    stateselect.append($('<option value="' + i + '">' + data[i] + '</option>'));
				}
            },
            'json'
		);
	});
});