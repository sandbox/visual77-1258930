<?php

function statepicker_admin_menu() {
  $menu['admin/settings/statepicker'] = array(
    'type' => MENU_NORMAL_ITEM,
    'title' => t('Statepicker'),
    'description' => t('Configure the list of states and countries for statepicker form items'),
    'page callback' => 'statepicker_country_list',
    'access arguments' => array('admin statepicker'),
    'file' => 'admin.inc.php',
  );
  $menu['admin/settings/statepicker/list'] = array(
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'title' => t('View'),
    'description' => t('View all countries'),
    'page callback' => 'statepicker_country_list',
    'access arguments' => array('admin statepicker'),
    'file' => 'admin.inc.php',
  );
  $menu['admin/settings/statepicker/add'] = array(
    'type' => MENU_LOCAL_TASK,
    'title' => t('Add'),
    'description' => t('Add a new country'),
    'page callback' => 'statepicker_country_add',
    'access arguments' => array('admin statepicker'),
    'file' => 'admin.inc.php',
  );
  $menu['admin/settings/statepicker/country/%'] = array(
    'type' => MENU_CALLBACK,
    'title' => t('List States'),
    'page callback' => 'statepicker_state_list',
    'page arguments' => array(4),
    'access arguments' => array('admin statepicker'),
    'file' => 'admin.inc.php',
  );
  $menu['admin/settings/statepicker/country/%/list'] = array(
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'title' => t('List States'),
    'page callback' => 'statepicker_state_list',
    'page arguments' => array(4),
    'access arguments' => array('admin statepicker'),
    'file' => 'admin.inc.php',
  );
  $menu['admin/settings/statepicker/country/%/edit'] = array(
    'type' => MENU_LOCAL_TASK,
    'title' => t('Edit'),
    'description' => t('Edit a country'),
    'page callback' => 'statepicker_country_edit',
    'page arguments' => array(4),
    'access arguments' => array('admin statepicker'),
    'file' => 'admin.inc.php',
  );
  $menu['admin/settings/statepicker/country/%/delete'] = array(
    'type' => MENU_LOCAL_TASK,
    'title' => t('Delete'),
    'description' => t('Delete a country'),
    'page callback' => 'statepicker_country_delete',
    'page arguments' => array(4),
    'access arguments' => array('admin statepicker'),
    'file' => 'admin.inc.php',
  );
  $menu['admin/settings/statepicker/country/%/disable'] = array(
    'type' => MENU_LOCAL_TASK,
    'title' => t('Disable'),
    'description' => t('Enable a country'),
    'page callback' => 'statepicker_country_disable',
    'page arguments' => array(4),
    'access arguments' => array('admin statepicker'),
    'file' => 'admin.inc.php',
  );
  $menu['admin/settings/statepicker/country/%/enable'] = array(
    'type' => MENU_LOCAL_TASK,
    'title' => t('Enable'),
    'description' => t('Enable a country'),
    'page callback' => 'statepicker_country_enable',
    'page arguments' => array(4),
    'access arguments' => array('admin statepicker'),
    'file' => 'admin.inc.php',
  );
  $menu['admin/settings/statepicker/country/%/add_state'] = array(
    'type' => MENU_LOCAL_TASK,
    'title' => t('Add State'),
    'page callback' => 'statepicker_state_add',
    'page arguments' => array(4),
    'access arguments' => array('administer statepicker'),
    'file' => 'admin.inc.php',
  );
  $menu['admin/settings/statepicker/state/%/edit'] = array(
    'type' => MENU_CALLBACK,
    'title' => t('Edit State'),
    'page callback' => 'statepicker_state_edit',
    'page arguments' => array(4),
    'access arguments' => array('administer statepicker'),
    'file' => 'admin.inc.php',
  );
  $menu['admin/settings/statepicker/state/%/delete'] = array(
    'type' => MENU_CALLBACK,
    'title' => t('Delete State'),
    'page callback' => 'statepicker_state_delete',
    'page arguments' => array(4),
    'access arguments' => array('administer statepicker'),
    'file' => 'admin.inc.php',
  );
  $menu['admin/settings/statepicker/state/%/disable'] = array(
    'type' => MENU_CALLBACK,
    'title' => t('Disable State'),
    'page callback' => 'statepicker_state_disable',
    'page arguments' => array(4),
    'access arguments' => array('administer statepicker'),
    'file' => 'admin.inc.php',
  );
  $menu['admin/settings/statepicker/state/%/enable'] = array(
    'type' => MENU_CALLBACK,
    'title' => t('Enable State'),
    'page callback' => 'statepicker_state_enable',
    'page arguments' => array(4),
    'access arguments' => array('administer statepicker'),
    'file' => 'admin.inc.php',
  );
  
  return $menu;
}

function statepicker_country_list() {
  $header = array(
    t('ID'),
    t('Name'),
    t('Continent'),
    t('Number of States'),
    array('data' => t('Operations'), 'colspan' => 4),
  );
  
  $enabled_country_result = db_query('select * from {statepicker_country} where enabled = 1 order by name asc');
  
  while($country = db_fetch_object($enabled_country_result)) {
    $enabled_rows[] = array(
      $country->cid,
      $country->name,
      $country->continent,
      _statepicker_get_state_count($country->cid),
      l(t('Edit'), 'admin/settings/statepicker/country/' . $country->cid . '/edit'),
      l(t('Delete'), 'admin/settings/statepicker/country/' . $country->cid . '/delete'),
      l(t('States'), 'admin/settings/statepicker/country/' . $country->cid),
      l(t('Disable'), 'admin/settings/statepicker/country/' . $country->cid . '/disable')
    );
  }
  
  $output  = '<h3>' . t('Enabled Countries') . '</h3>';
  $output .= theme('table', $header, $enabled_rows);

  $disabled_country_result = db_query('select * from {statepicker_country} where enabled = 0 order by name asc');
  
  while($country = db_fetch_object($disabled_country_result)) {
    $disabled_rows[] = array(
      $country->cid,
      $country->name,
      $country->continent,
      _statepicker_get_state_count($country->cid),
      l(t('Edit'), 'admin/settings/statepicker/country/' . $country->cid . '/edit'),
      l(t('Delete'), 'admin/settings/statepicker/country/' . $country->cid . '/delete'),
      l(t('States'), 'admin/settings/statepicker/country/' . $country->cid),
      l(t('Enable'), 'admin/settings/statepicker/country/' . $country->cid . '/enable')
    );
  }
  
  $output .= '<h3>' . t('Disabled Countries') . '</h3>';
  $output .= theme('table', $header, $disabled_rows);
  
  return $output;
}

function statepicker_country_add() {
  return drupal_get_form('statepicker_country_form');
}

function statepicker_country_edit($cid) {
  return drupal_get_form('statepicker_country_form', _statepicker_fetch_country($cid));
}

function statepicker_country_delete($cid, $confirm = false) {
  $country = _statepicker_fetch_country($cid);
  if($confirm === false) {
    $output  = '<p>Are you sure you want to delete this country? All of this country\'s states will be deleted as well.</p>';
    $output .= '<dl>';
    $output .= '<dt>' . t('ID') . '</dt>';
    $output .= '<dd>' . $country->cid . '</dd>';
    $output .= '<dt>' . t('Name') . '</dt>';
    $output .= '<dd>' . $country->name . '</dd>';
    $output .= '<dt>' . t('Continent') . '</dt>';
    $output .= '<dd>' . $country->continent . '</dd>';
    $output .= '<dt>' . t('Number of States') . '</dt>';
    $output .= '<dd>' . _statepicker_get_state_count($country->cid) . '</dd>';
    $output .= '</dl>';
    $output .= '<p>To confirm this deletion, ' . l('click here', 'admin/settings/statepicker/country/' . $country->cid . '/delete/confirm');
    return $output;
  } else {
    db_query('delete from {statepicker_country} where cid = "%d"', $country->cid);
    db_query('delete from {statepicker_state} where cid = "%d"', $country->cid);
    drupal_goto('admin/settings/statepicker');
  }
}

function statepicker_country_disable($cid) {
  _statepicker_country_toggle($cid, 0);
}

function statepicker_country_enable($cid) {
  _statepicker_country_toggle($cid, 1);
}

function _statepicker_country_toggle($cid, $enabled) {
  db_query(
    'UPDATE {statepicker_country} SET enabled = "%d" WHERE cid = "%d"',
    $enabled,
    $cid
  );
  drupal_goto('admin/settings/statepicker');
}

function statepicker_country_form(&$form_state, $country = false) {
  $form = array(
    '#type' => 'fieldset',
    '#title' => t('Edit Country'),
  );
  $form['cid'] = array(
    '#type' => 'hidden',
    '#value' => $country->cid,
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $country->name,
  );
  $form['continent'] = array(
    '#type' => 'select',
    '#title' => t('Continent'),
    '#options' => array(
      'AF' => 'Africa',
      'AN' => 'Antarctica',
      'AS' => 'Asia',
      'EU' => 'Europe',
      'NA' => 'North America',
      'OC' => 'Oceania',
      'SA' => 'South America',
    ),
    '#default_value' => $country->continent,
  );
  $form['iso'] = array(
    '#type' => 'textfield',
    '#title' => t('ISO 3166-2'),
    '#default_value' => $country->iso,
    '#description' => 'More info ' . l('here', 'http://en.wikipedia.org/wiki/ISO_3166-2') . '.',
  );
  $form['iso3'] = array(
    '#type' => 'textfield',
    '#title' => t('ISO 3166-1 alpha-3'),
    '#default_value' => $country->iso3,
    '#description' => 'More info ' . l('here', 'http://en.wikipedia.org/wiki/ISO_3166-1_alpha-3') . '.',
  );
  $form['fips'] = array(
    '#type' => 'textfield',
    '#title' => t('FIPS'),
    '#description' => t('Federal Information Processing Standard region code. More info ' . l('here', 'http://en.wikipedia.org/wiki/List_of_FIPS_country_codes') . '.'),
    '#default_value' => $country->fips,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save Country',
    '#description' => t('Save this country. Perhaps save the world next?'),
  );
  
  return $form;
}

function statepicker_country_form_submit($form, &$form_state) {
  if($form_state['values']['cid'] > 0) {
    db_query(
      'update {statepicker_country} set name = "%s", continent = "%s", iso = "%s", iso3 = "%s", fips = "%s" where cid = "%d"',
      $form_state['values']['name'],
      $form_state['values']['continent'],
      $form_state['values']['iso'],
      $form_state['values']['iso3'],
      $form_state['values']['fips'],
      $form_state['values']['cid']
    );
  } else {
    db_query(
      'insert into {statepicker_country} (name, continent, iso, iso3, fips) values ("%s", "%s", "%s", "%s", "%s")',
      $form_state['values']['name'],
      $form_state['values']['continent'],
      $form_state['values']['iso'],
      $form_state['values']['iso3'],
      $form_state['values']['fips']
    );
  }
  $form_state['redirect'] = 'admin/settings/statepicker/country/list';
  return $form_state;
}

function statepicker_state_list($cid) {
  $country = _statepicker_fetch_country($cid);
  
  $header = array(t('Name'), t('Timezone'), array('data' => 'Operations', 'colspan' => 3));
  
  $state_result = db_query('select * from {statepicker_state} where cid = "%d" and enabled = 1', $cid);
  
  while($state = db_fetch_object($state_result)) {
    $enabled_rows[] = array(
      $state->name,
      $state->timezone,
      l('Edit', 'admin/settings/statepicker/state/' .  $state->sid . '/edit'),
      l('Delete', 'admin/settings/statepicker/state/' . $state->sid . '/delete'),
      l('Disable', 'admin/settings/statepicker/state/' . $state->sid . '/disable'),
    );
  }
  
  $output = '<h3>Viewing States for ' . $country->name . '</h3>';
  $output .= '<h4>' . t('Enabled States') . '</h4>';
  $output .= theme('table', $header, $enabled_rows);
  
  $state_result = db_query('select * from {statepicker_state} where cid = "%d" and enabled = 0', $cid);
  
  while($state = db_fetch_object($state_result)) {
    $disabled_rows[] = array(
      $state->name,
      $state->timezone,
      l('Edit', 'admin/settings/statepicker/state/' .  $state->sid . '/edit'),
      l('Delete', 'admin/settings/statepicker/state/' . $state->sid . '/delete'),
      l('Enable', 'admin/settings/statepicker/state/' . $state->sid . '/enable'),
    );
  }
  
  $output .= '<h4>' . t('Disabled States') . '</h4>';
  $output .= theme('table', $header, $disabled_rows);
  
  return $output;
}

function statepicker_state_add($cid) {
  $state = new StdClass();
  $state->cid = $cid;
  return drupal_get_form('statepicker_state_form', $state);
}

function statepicker_state_edit($sid) {
  return drupal_get_form('statepicker_state_form', _statepicker_fetch_state($sid));
}

function statepicker_state_delete($sid, $confirm = false) {
  $state = _statepicker_fetch_state($sid);
  if($confirm === false) {
    $output  = '<p>Are you sure you want to delete this state?</p>';
    $output .= '<dl>';
    $output .= '<dt>' . t('ID') . '</dt>';
    $output .= '<dd>' . $state->sid . '</dd>';
    $output .= '<dt>' . t('Name') . '</dt>';
    $output .= '<dd>' . $state->name . '</dd>';
    $output .= '<dt>' . t('Timezone') . '</dt>';
    $output .= '<dd>' . $state->timezone . '</dd>';
    $output .= '<dt>' . t('Country') . '</dt>';
    $output .= '<dd>' . _statepicker_fetch_country($state->cid)->name . '</dd>';
    $output .= '</dl>';
    $output .= '<p>To confirm this deletion, ' . l('click here', 'admin/settings/statepicker/state/' . $state->sid . '/delete/confirm');
    return $output;
  } else {
    db_query('delete from {statepicker_state} where sid = "%d"', $state->sid);
    drupal_goto('admin/settings/statepicker/country/' . $state->cid);
  }
}

function statepicker_state_disable($sid) {
  _statepicker_state_toggle($sid, 0);
}

function statepicker_state_enable($sid) {
  _statepicker_state_toggle($sid, 1);
}

function _statepicker_state_toggle($sid, $enabled) {
  db_query(
    'UPDATE {statepicker_state} SET enabled = "%d" WHERE sid = "%d"',
    $enabled,
    $sid
  );
  drupal_goto('admin/settings/statepicker/country/' . _statepicker_fetch_state($sid)->cid);
}

function statepicker_state_form(&$form_state, $state = false) {
  $form = array(
    '#type' => 'fieldset',
    '#title' => t('Edit State'),
  );
  
  $form['sid'] = array(
    '#type' => 'hidden',
    '#value' => $state->sid,
  );
  $form['cid'] = array(
    '#type' => 'hidden',
    '#value' => $state->cid,
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $state->name,
  );
  $form['timezone'] = array(
    '#type' => 'textfield',
    '#title' => t('Timezome'),
    '#description' => t('Timezone as defined in the ' . l('tz database', 'http://en.wikipedia.org/wiki/Tz_database') . ', such as America/Los_Angeles'),
    '#default_value' => $state->timezone,
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save State'),
  );
  
  return $form;
}

function statepicker_state_form_validate($form, &$form_state) {
  if( ! @timezone_open($form_state['values']['timezone'])) {
    form_set_error('timezone', t('Invalid timezone'));
  }
}

function statepicker_state_form_submit($form, &$form_state) {
  if($form_state['values']['sid'] > 0) {
    db_query(
      'update {statepicker_state} set name = "%s", timezone = "%s" where sid = "%d"',
      $form_state['values']['name'],
      $form_state['values']['timezone'],
      $form_state['values']['sid']
    );
  } else {
    db_query(
      'insert into {statepicker_state} (name, timezone, cid) values ("%s", "%s", "%d")',
      $form_state['values']['name'],
      $form_state['values']['timezone'],
      $form_state['values']['cid']
    );
  }
  $form_state['redirect'] = 'admin/settings/statepicker/country/' . $form_state['values']['cid'];
  return $form_state;
}

function _statepicker_get_state_count($cid) {
  $state_count = db_fetch_object(db_query(
    'select count(*) as count from {statepicker_state} where cid = "%d"', 
    $cid
  ));
  return $state_count->count;
}

function _statepicker_fetch_country($cid) {
  return db_fetch_object(db_query('select * from {statepicker_country} where cid = "%d"', $cid));
}

function _statepicker_fetch_state($sid) {
  return db_fetch_object(db_query('select * from {statepicker_state} where sid = "%d"', $sid));
}